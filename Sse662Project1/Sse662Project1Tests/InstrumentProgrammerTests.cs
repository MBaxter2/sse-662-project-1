﻿using System;
using System.Threading;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sse662Project1;


namespace Sse662Project1Tests
{
    [TestClass]
    public class InstrumentProgrammerTests
    {
        [TestMethod]
        public void InstrumentProgrammerTest1()
        {
            var ip = new InstrumentProgrammer();

            Assert.AreEqual(ip.Period, 100);
            Assert.AreEqual(ip.LastError, "");

            for (int i = 0; i < 5; i++)
            {
                Assert.AreEqual(ip.ChannelVoltages[i].Number, i+1);
                Assert.AreEqual(ip.ChannelVoltages[i].High, 5.0);
                Assert.AreEqual(ip.ChannelVoltages[i].Low, 1.0);
                Assert.AreEqual(ip.ChannelVoltages[i].Detect, 2.5);
                Assert.AreEqual(ip.ChannelLogic[i].Number, i + 1);
                Assert.AreEqual(ip.ChannelResults[i].Number, i + 1);
                for (int j = 0; j < 10; j++)
                {
                    Assert.AreEqual(ip.ChannelLogic[i].Patterns[j].DriverLevel, LogicLevel.Disabled);
                    Assert.AreEqual(ip.ChannelLogic[i].Patterns[j].DetectLevel, LogicLevel.Disabled);
                    Assert.AreEqual(ip.ChannelResults[i].Results[j], TestResult.NotTested);
                }
            }
        }

        [TestMethod]
        public void InstrumentProgrammerTest2()
        {
            var ip = new InstrumentProgrammer();

            ip.ChannelVoltages[0].High = 40;
            ip.RunConfiguration();
            Assert.AreEqual(String.IsNullOrEmpty(ip.LastError), false);
        }

        [TestMethod]
        public void InstrumentProgrammerTest3()
        {
            var ip = new InstrumentProgrammer();

            for (int i = 0; i < 10; i++)
            {
                ip.ChannelLogic[0].Patterns[i].DetectLevel = LogicLevel.Low;
                ip.ChannelLogic[0].Patterns[i].DriverLevel = LogicLevel.Low;
            }

            for (int i = 0; i < 10; i++)
            {
                ip.ChannelLogic[1].Patterns[i].DetectLevel = LogicLevel.High;
                ip.ChannelLogic[1].Patterns[i].DriverLevel = LogicLevel.High;
            }

            for (int i = 0; i < 10; i++)
            {
                if(i % 2 == 0)
                {
                    ip.ChannelLogic[2].Patterns[i].DetectLevel = LogicLevel.Low;
                    ip.ChannelLogic[2].Patterns[i].DriverLevel = LogicLevel.Low;
                }
                else
                {
                    ip.ChannelLogic[2].Patterns[i].DetectLevel = LogicLevel.High;
                    ip.ChannelLogic[2].Patterns[i].DriverLevel = LogicLevel.High;
                }
            }

            for (int i = 0; i < 10; i++)
            {
                if (i % 2 == 0)
                {
                    ip.ChannelLogic[3].Patterns[i].DetectLevel = LogicLevel.High;
                    ip.ChannelLogic[3].Patterns[i].DriverLevel = LogicLevel.High;
                }
                else
                {
                    ip.ChannelLogic[3].Patterns[i].DetectLevel = LogicLevel.Low;
                    ip.ChannelLogic[3].Patterns[i].DriverLevel = LogicLevel.Low;
                }
            }

            ip.RunConfiguration();

            Assert.AreEqual(ip.ChannelResults[0].Results[0], TestResult.Passed);
            Assert.AreEqual(ip.ChannelResults[0].Results[1], TestResult.Failed);
            Assert.AreEqual(ip.ChannelResults[0].Results[2], TestResult.Passed);
            Assert.AreEqual(ip.ChannelResults[0].Results[3], TestResult.Failed);
            Assert.AreEqual(ip.ChannelResults[0].Results[4], TestResult.Passed);
            Assert.AreEqual(ip.ChannelResults[0].Results[5], TestResult.Failed);
            Assert.AreEqual(ip.ChannelResults[0].Results[6], TestResult.Passed);
            Assert.AreEqual(ip.ChannelResults[0].Results[7], TestResult.Passed);
            Assert.AreEqual(ip.ChannelResults[0].Results[8], TestResult.Passed);
            Assert.AreEqual(ip.ChannelResults[0].Results[9], TestResult.Failed);

            Assert.AreEqual(ip.ChannelResults[1].Results[0], TestResult.Failed);
            Assert.AreEqual(ip.ChannelResults[1].Results[1], TestResult.Passed);
            Assert.AreEqual(ip.ChannelResults[1].Results[2], TestResult.Failed);
            Assert.AreEqual(ip.ChannelResults[1].Results[3], TestResult.Passed);
            Assert.AreEqual(ip.ChannelResults[1].Results[4], TestResult.Failed);
            Assert.AreEqual(ip.ChannelResults[1].Results[5], TestResult.Passed);
            Assert.AreEqual(ip.ChannelResults[1].Results[6], TestResult.Failed);
            Assert.AreEqual(ip.ChannelResults[1].Results[7], TestResult.Failed);
            Assert.AreEqual(ip.ChannelResults[1].Results[8], TestResult.Failed);
            Assert.AreEqual(ip.ChannelResults[1].Results[9], TestResult.Passed);

            Assert.AreEqual(ip.ChannelResults[2].Results[0], TestResult.Passed);
            Assert.AreEqual(ip.ChannelResults[2].Results[1], TestResult.Passed);
            Assert.AreEqual(ip.ChannelResults[2].Results[2], TestResult.Passed);
            Assert.AreEqual(ip.ChannelResults[2].Results[3], TestResult.Passed);
            Assert.AreEqual(ip.ChannelResults[2].Results[4], TestResult.Passed);
            Assert.AreEqual(ip.ChannelResults[2].Results[5], TestResult.Passed);
            Assert.AreEqual(ip.ChannelResults[2].Results[6], TestResult.Passed);
            Assert.AreEqual(ip.ChannelResults[2].Results[7], TestResult.Failed);
            Assert.AreEqual(ip.ChannelResults[2].Results[8], TestResult.Passed);
            Assert.AreEqual(ip.ChannelResults[2].Results[9], TestResult.Passed);

            Assert.AreEqual(ip.ChannelResults[3].Results[0], TestResult.Failed);
            Assert.AreEqual(ip.ChannelResults[3].Results[1], TestResult.Failed);
            Assert.AreEqual(ip.ChannelResults[3].Results[2], TestResult.Failed);
            Assert.AreEqual(ip.ChannelResults[3].Results[3], TestResult.Failed);
            Assert.AreEqual(ip.ChannelResults[3].Results[4], TestResult.Failed);
            Assert.AreEqual(ip.ChannelResults[3].Results[5], TestResult.Failed);
            Assert.AreEqual(ip.ChannelResults[3].Results[6], TestResult.Failed);
            Assert.AreEqual(ip.ChannelResults[3].Results[7], TestResult.Passed);
            Assert.AreEqual(ip.ChannelResults[3].Results[8], TestResult.Failed);
            Assert.AreEqual(ip.ChannelResults[3].Results[9], TestResult.Failed);

            Assert.AreEqual(ip.ChannelResults[4].Results[0], TestResult.NotTested);
            Assert.AreEqual(ip.ChannelResults[4].Results[1], TestResult.NotTested);
            Assert.AreEqual(ip.ChannelResults[4].Results[2], TestResult.NotTested);
            Assert.AreEqual(ip.ChannelResults[4].Results[3], TestResult.NotTested);
            Assert.AreEqual(ip.ChannelResults[4].Results[4], TestResult.NotTested);
            Assert.AreEqual(ip.ChannelResults[4].Results[5], TestResult.NotTested);
            Assert.AreEqual(ip.ChannelResults[4].Results[6], TestResult.NotTested);
            Assert.AreEqual(ip.ChannelResults[4].Results[7], TestResult.NotTested);
            Assert.AreEqual(ip.ChannelResults[4].Results[8], TestResult.NotTested);
            Assert.AreEqual(ip.ChannelResults[4].Results[9], TestResult.NotTested);
        }

        [TestMethod]
        public void InstrumentProgrammerTest4()
        {
            const string fileName = "Patterns.xml";
            var ip = new InstrumentProgrammer();

            if (File.Exists(fileName))
            {
                File.Delete(fileName);
                Thread.Sleep(50);
            }

            ip.SaveConfiguration();
            Thread.Sleep(50);

            Assert.AreEqual(File.Exists(fileName), true);
        }

        [TestMethod]
        public void InstrumentProgrammerTest5()
        {
            var ip = new InstrumentProgrammer();

            ip.ChannelLogic[0].Patterns[0].DetectLevel = LogicLevel.High;
            ip.ChannelLogic[0].Patterns[0].DriverLevel = LogicLevel.Low;

            ip.SaveConfiguration();
            Thread.Sleep(50);

            ip = new InstrumentProgrammer();
            ip.LoadConfiguration();

            Assert.AreEqual(ip.ChannelLogic[0].Patterns[0].DetectLevel, LogicLevel.High);
            Assert.AreEqual(ip.ChannelLogic[0].Patterns[0].DriverLevel, LogicLevel.Low);
        }
    }
}
