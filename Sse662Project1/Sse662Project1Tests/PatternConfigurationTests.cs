﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sse662Project1;

namespace Sse662Project1Tests
{
    [TestClass]
    public class PatternConfigurationTests
    {
        string propertyChanged;

        void testPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            propertyChanged = e.PropertyName;
        }

        [TestMethod]
        public void PatternDefault()
        {
            var pattern = new PatternConfiguration();

            Assert.AreEqual(LogicLevel.Disabled, pattern.DetectLevel);
            Assert.AreEqual(LogicLevel.Disabled, pattern.DriverLevel);
        }

        [TestMethod]
        public void PatternModify()
        {
            var pattern = new PatternConfiguration();

            pattern.DetectLevel = LogicLevel.High;
            pattern.DriverLevel = LogicLevel.Low;

            Assert.AreEqual(LogicLevel.High, pattern.DetectLevel);
            Assert.AreEqual(LogicLevel.Low, pattern.DriverLevel);
        }

        [TestMethod]
        public void PatternModify2()
        {
            var pattern = new PatternConfiguration();

            pattern.PropertyChanged += testPropertyChanged;
            propertyChanged = "";
            pattern.DetectLevel = LogicLevel.Low;

            Assert.AreEqual(propertyChanged, "DetectLevel");
        }

        [TestMethod]
        public void PatternModify3()
        {
            var pattern = new PatternConfiguration();

            pattern.PropertyChanged += testPropertyChanged;
            propertyChanged = "";
            pattern.DriverLevel = LogicLevel.High;

            Assert.AreEqual(propertyChanged, "DriverLevel");
        }
    }
}
