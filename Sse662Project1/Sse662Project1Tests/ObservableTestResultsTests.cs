﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sse662Project1;
using System.Collections.ObjectModel;

namespace Sse662Project1Tests
{
    [TestClass]
    public class ObservableTestResultsTests
    {
        string propertyChanged;

        void testPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            propertyChanged = e.PropertyName;
        }

        [TestMethod]
        public void ObservableTestResultsTest1()
        {
            var otr = new ObservableTestResults(3, 2, TestResult.Failed);

            Assert.AreEqual(otr.Number, 3);
            for (int i = 0; i < 2; i++)
                Assert.AreEqual(otr.Results[i], TestResult.Failed);
        }

        [TestMethod]
        public void ObservableTestResultsTest2()
        {
            var otr = new ObservableTestResults(0, 2, TestResult.NotTested);

            otr.PropertyChanged += testPropertyChanged;
            propertyChanged = "";
            otr.Number = 0;

            Assert.AreEqual(propertyChanged, "Number");            
        }

        [TestMethod]
        public void ObservableTestResultsTest3()
        {
            var otr = new ObservableTestResults(0, 2, TestResult.NotTested);

            otr.PropertyChanged += testPropertyChanged;
            propertyChanged = "";
            otr.Results = new ObservableCollection<TestResult>();

            Assert.AreEqual(propertyChanged, "Results");
        }
    }
}
