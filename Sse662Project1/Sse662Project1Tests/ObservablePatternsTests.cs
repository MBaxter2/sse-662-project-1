﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sse662Project1;
using System.Collections.ObjectModel;

namespace Sse662Project1Tests
{
    [TestClass]
    public class ObservablePatternsTests
    {
        string propertyChanged;

        void testPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            propertyChanged = e.PropertyName;
        }

        [TestMethod]
        public void ObservablePatternTest1()
        {
            var op = new ObservablePatterns();

            Assert.AreEqual(op.Number,0);
        }

        [TestMethod]
        public void ObservablePatternTest2()
        {
            var op = new ObservablePatterns(3, 2, LogicLevel.High);

            Assert.AreEqual(op.Number, 3);
            for (int i = 0; i < 2; i++) {
                Assert.AreEqual(op.Patterns[i].DetectLevel, LogicLevel.High);
                Assert.AreEqual(op.Patterns[i].DriverLevel, LogicLevel.High);
            }

        }

        [TestMethod]
        public void ObservablePatternTest3()
        {
            var op = new ObservablePatterns(3, 2, LogicLevel.High);

            op.PropertyChanged += testPropertyChanged;
            propertyChanged = "";
            op.Number = 0;

            Assert.AreEqual(propertyChanged, "Number");
        }

        [TestMethod]
        public void ObservablePatternTest4()
        {
            var op = new ObservablePatterns(3, 2, LogicLevel.High);

            op.PropertyChanged += testPropertyChanged;
            propertyChanged = "";
            op.Patterns = new ObservableCollection<PatternConfiguration>();

            Assert.AreEqual(propertyChanged, "Patterns");
        }
    }
}
