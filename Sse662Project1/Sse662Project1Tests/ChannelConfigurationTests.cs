﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sse662Project1;

namespace Sse662Project1Tests
{
    [TestClass]
    public class ChannelConfigurationTests
    {
        [TestMethod]
        public void ChannelDefault()
        {
            var channel = new ChannelConfiguration();

            Assert.AreEqual(5.0, channel.DriverHiVoltage);
            Assert.AreEqual(0.8, channel.DriverLoVoltage);
            Assert.AreEqual(2.2, channel.DetectThresholdVoltage);
        }

        [TestMethod]
        public void ChannelModify()
        {
            var channel = new ChannelConfiguration();

            channel.DriverHiVoltage = 8.0;
            channel.DriverLoVoltage = 0.1;
            channel.DetectThresholdVoltage = 4.0;

            Assert.AreEqual(8.0, channel.DriverHiVoltage);
            Assert.AreEqual(0.1, channel.DriverLoVoltage);
            Assert.AreEqual(4.0, channel.DetectThresholdVoltage);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException), "Voltage setting cannot be above max value of 10.0")]
        public void ChannelHiOverVoltage()
        {
            var channel = new ChannelConfiguration();

            channel.DriverHiVoltage = 11.0;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException), "Voltage setting cannot be below min value of 0.0")]
        public void ChannelHiUnderVoltage()
        {
            var channel = new ChannelConfiguration();

            channel.DriverHiVoltage = -1.0;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException), "Voltage setting cannot be below Driver Low Voltage")]
        public void ChannelHiBelowLoVoltage()
        {
            var channel = new ChannelConfiguration();

            channel.DriverHiVoltage = 0.1;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException), "Voltage setting cannot be above max value of 10.0")]
        public void ChannelLoOverVoltage()
        {
            var channel = new ChannelConfiguration();

            channel.DriverLoVoltage = 11.0;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException), "Voltage setting cannot be below min value of 0.0")]
        public void ChannelLoUnderVoltage()
        {
            var channel = new ChannelConfiguration();

            channel.DriverLoVoltage = -1.0;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException), "Voltage setting cannot be above Driver Hi Voltage")]
        public void ChannelLoAboveHiVoltage()
        {
            var channel = new ChannelConfiguration();

            channel.DriverLoVoltage = 5.1;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException), "Voltage setting cannot be above max value of 10.0")]
        public void ChannelDetectOverVoltage()
        {
            var channel = new ChannelConfiguration();

            channel.DetectThresholdVoltage = 11.0;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException), "Voltage setting cannot be below min value of 0.0")]
        public void ChannelDetectUnderVoltage()
        {
            var channel = new ChannelConfiguration();

            channel.DetectThresholdVoltage = -1.0;
        }
    }
}
