﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sse662Project1;
using System.IO;

namespace Sse662Project1Tests
{
    [TestClass]
    public class ResponseSimulatorTests
    {
        const string SIM_FILE_FOLDER = @"..\..\Simulation Files\";

        [TestMethod]
        public void SimulatorDefault()
        {
            const int channels = 5;
            const int patterns = 10;
            var sim = new ResponseSimulator(channels, patterns);

            for (int i = 0; i < channels; i++)
                for (int j = 0; j < patterns; j++)
                    Assert.AreEqual(sim.Data[i, j], 0);
        }

        [TestMethod]
        public void SimulationTestFile1()
        {
            var sim = new ResponseSimulator(3, 1);

            sim.LoadSimulationFile(SIM_FILE_FOLDER + "sim1.xml");

            Assert.AreEqual(sim.Data[0, 0], 1.0);
            Assert.AreEqual(sim.Data[1, 0], 2.0);
            Assert.AreEqual(sim.Data[2, 0], 3.0);            
        }

        [TestMethod]
        public void SimulationTestFile2()
        {
            var sim = new ResponseSimulator(6, 10);

            sim.LoadSimulationFile(SIM_FILE_FOLDER + "sim2.xml");

            Assert.AreEqual(sim.Data[0, 0], 0.0);
            Assert.AreEqual(sim.Data[3, 5], 6.0);
            Assert.AreEqual(sim.Data[4, 9], 3);
        }

        [TestMethod]
        [ExpectedException(typeof(SimulatorFileException), "File exceeds number of patterns")]
        public void SimulationTestFile3()
        {
            var sim = new ResponseSimulator(6, 10);

            sim.LoadSimulationFile(SIM_FILE_FOLDER + "sim3.xml");
        }

        [TestMethod]
        [ExpectedException(typeof(SimulatorFileException), "File exceeds number of channels")]
        public void SimulationTestFile4()
        {
            var sim = new ResponseSimulator(6, 10);

            sim.LoadSimulationFile(SIM_FILE_FOLDER + "sim4.xml");
        }

        [TestMethod]
        [ExpectedException(typeof(SimulatorFileException), "Invalid simulation file format")]
        public void SimulationTestFile5()
        {
            var sim = new ResponseSimulator(2, 1);

            sim.LoadSimulationFile(SIM_FILE_FOLDER + "sim5.xml");
        }
    }
}
