﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sse662Project1;

namespace Sse662Project1Tests
{
    [TestClass]
    public class DigitalDriverTests
    {
        [TestMethod]
        public void SetChannelDriverVoltageHiTest1()
        {
            var driver = new DigitalDriver();
            driver.SetChannelDriverVoltageHi(0, 5.0);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException), "Invalid channel number")]
        public void SetChannelDriverVoltageHiTest2()
        {
            var driver = new DigitalDriver();
            driver.SetChannelDriverVoltageHi(-1, 5.0);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException), "Invalid channel number")]
        public void SetChannelDriverVoltageHiTest3()
        {
            var driver = new DigitalDriver();
            driver.SetChannelDriverVoltageHi(5, 5.0);
        }

        [TestMethod]
        public void SetChannelDriverVoltageLoTest1()
        {
            var driver = new DigitalDriver();
            driver.SetChannelDriverVoltageLo(0, 0.0);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException), "Invalid channel number")]
        public void SetChannelDriverVoltageLoTest2()
        {
            var driver = new DigitalDriver();
            driver.SetChannelDriverVoltageLo(-1, 0.0);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException), "Invalid channel number")]
        public void SetChannelDriverVoltageLoTest3()
        {
            var driver = new DigitalDriver();
            driver.SetChannelDriverVoltageLo(5, 0.0);
        }

        [TestMethod]
        public void SetChannelDetectVoltageTest1()
        {
            var driver = new DigitalDriver();
            driver.SetChannelDetectVoltage(0, 4.0);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException), "Invalid channel number")]
        public void SetChannelDetectVoltageTest2()
        {
            var driver = new DigitalDriver();
            driver.SetChannelDetectVoltage(-1, 4.0);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException), "Invalid channel number")]
        public void SetChannelDetectVoltageTest3()
        {
            var driver = new DigitalDriver();
            driver.SetChannelDetectVoltage(5, 4.0);
        }

        [TestMethod]
        public void SetPatternPeriodTest1()
        {
            var driver = new DigitalDriver();
            driver.SetPatternPeriod(1.0);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException), "Period setting cannot be below min value of 1.0")]
        public void SetPatternPeriodTest2()
        {
            var driver = new DigitalDriver();
            driver.SetPatternPeriod(0.1);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException), "Period setting cannot be above max value of 1000000")]
        public void SetPatternPeriodTest3()
        {
            var driver = new DigitalDriver();
            driver.SetPatternPeriod(1000000.1);
        }

        [TestMethod]
        public void SetChannelDriverLogicTest1()
        {
            var driver = new DigitalDriver();
            driver.SetChannelDriverLogic(0, 1, LogicLevel.High);
            driver.SetChannelDriverLogic(3, 7, LogicLevel.Low);
            driver.SetChannelDriverLogic(4, 9, LogicLevel.Disabled);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException), "Invalid channel number")]
        public void SetChannelDriverLogicTest2()
        {
            var driver = new DigitalDriver();
            driver.SetChannelDriverLogic(6, 1, LogicLevel.High);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException), "Invalid pattern index")]
        public void SetChannelDriverLogicTest3()
        {
            var driver = new DigitalDriver();
            driver.SetChannelDriverLogic(0, 10, LogicLevel.High);
        }

        [TestMethod]
        public void DriverExecuteTest1()
        {
            var driver = new DigitalDriver();

            for (int i = 0; i < 5; i++) driver.SetChannelDetectVoltage(i, 2.0);
            for (int i = 0; i < 5; i++) driver.SetChannelDetectLogic(i, 0, LogicLevel.Low);
            for (int i = 0; i < 5; i++) driver.SetChannelDetectLogic(i, 1, LogicLevel.High);
            for (int i = 0; i < 5; i++) driver.SetChannelDetectLogic(i, 2, LogicLevel.Low);
            for (int i = 0; i < 5; i++) driver.SetChannelDetectLogic(i, 3, LogicLevel.High);
            for (int i = 0; i < 5; i++) driver.SetChannelDetectLogic(i, 4, LogicLevel.Disabled);
            for (int i = 0; i < 5; i++) driver.SetChannelDetectLogic(i, 5, LogicLevel.High);
            for (int i = 0; i < 5; i++) driver.SetChannelDetectLogic(i, 6, LogicLevel.High);
            for (int i = 0; i < 5; i++) driver.SetChannelDetectLogic(i, 7, LogicLevel.High);
            for (int i = 0; i < 5; i++) driver.SetChannelDetectLogic(i, 8, LogicLevel.Low);
            for (int i = 0; i < 5; i++) driver.SetChannelDetectLogic(i, 9, LogicLevel.Low);

            int result = driver.Execute();

            Assert.AreEqual(result, 15);
        }

        [TestMethod]
        public void GetChannelResultTest1()
        {
            var driver = new DigitalDriver();
            Assert.AreEqual(driver.GetChannelResult(2, 4), TestResult.NotTested);
        }

        [TestMethod]
        public void GetChannelResultTest2()
        {
            var driver = new DigitalDriver();

            for (int i = 0; i < 5; i++) driver.SetChannelDetectVoltage(i, 2.0);
            for (int i = 0; i < 5; i++) driver.SetChannelDetectLogic(i, 0, LogicLevel.Low);
            for (int i = 0; i < 5; i++) driver.SetChannelDetectLogic(i, 1, LogicLevel.High);
            for (int i = 0; i < 5; i++) driver.SetChannelDetectLogic(i, 2, LogicLevel.Low);
            for (int i = 0; i < 5; i++) driver.SetChannelDetectLogic(i, 3, LogicLevel.High);
            for (int i = 0; i < 5; i++) driver.SetChannelDetectLogic(i, 4, LogicLevel.Disabled);
            for (int i = 0; i < 5; i++) driver.SetChannelDetectLogic(i, 5, LogicLevel.High);
            for (int i = 0; i < 5; i++) driver.SetChannelDetectLogic(i, 6, LogicLevel.High);
            for (int i = 0; i < 5; i++) driver.SetChannelDetectLogic(i, 7, LogicLevel.High);
            for (int i = 0; i < 5; i++) driver.SetChannelDetectLogic(i, 8, LogicLevel.Low);
            for (int i = 0; i < 5; i++) driver.SetChannelDetectLogic(i, 9, LogicLevel.Low);

            driver.Execute();

            Assert.AreEqual(driver.GetChannelResult(0, 0), TestResult.Passed);
            Assert.AreEqual(driver.GetChannelResult(2, 4), TestResult.NotTested);
            Assert.AreEqual(driver.GetChannelResult(4, 9), TestResult.Failed);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException), "Invalid channel number")]
        public void GetChannelResultTest3()
        {
            var driver = new DigitalDriver();
            driver.GetChannelResult(5, 4);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException), "Invalid pattern index")]
        public void GetChannelResultTest4()
        {
            var driver = new DigitalDriver();
            driver.GetChannelResult(2, 10);
        }
    }
}
