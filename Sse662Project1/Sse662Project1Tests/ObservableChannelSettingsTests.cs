﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sse662Project1;

namespace Sse662Project1Tests
{
    [TestClass]
    public class ObservableChannelSettingsTests
    {
        string propertyChanged;

        void testPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            propertyChanged = e.PropertyName;
        }

        [TestMethod]
        public void ObservableChannelSettingsTest1()
        {
            var ocs = new ObservableChannelSettings(1, 3.0, 2.0, 2.5);

            Assert.AreEqual(ocs.Number, 1);
            Assert.AreEqual(ocs.High, 3.0);
            Assert.AreEqual(ocs.Low, 2.0);
            Assert.AreEqual(ocs.Detect, 2.5);
        }

        [TestMethod]
        public void ObservableChannelSettingsTest2()
        {
            var ocs = new ObservableChannelSettings(1, 1.0, 1.0, 1.0);

            ocs.PropertyChanged += testPropertyChanged;
            propertyChanged = "";
            ocs.Number = 5;

            Assert.AreEqual(propertyChanged, "Number");
        }

        [TestMethod]
        public void ObservableChannelSettingsTest3()
        {
            var ocs = new ObservableChannelSettings(1, 1.0, 1.0, 1.0);

            ocs.PropertyChanged += testPropertyChanged;
            propertyChanged = "";
            ocs.High = 5;

            Assert.AreEqual(propertyChanged, "High");
        }

        [TestMethod]
        public void ObservableChannelSettingsTest4()
        {
            var ocs = new ObservableChannelSettings(1, 1.0, 1.0, 1.0);

            ocs.PropertyChanged += testPropertyChanged;
            propertyChanged = "";
            ocs.Low = 5;

            Assert.AreEqual(propertyChanged, "Low");
        }

        [TestMethod]
        public void ObservableChannelSettingsTest5()
        {
            var ocs = new ObservableChannelSettings(1, 1.0, 1.0, 1.0);

            ocs.PropertyChanged += testPropertyChanged;
            propertyChanged = "";
            ocs.Detect = 5;

            Assert.AreEqual(propertyChanged, "Detect");
        }
    }
}
