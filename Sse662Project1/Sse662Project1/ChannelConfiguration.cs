﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sse662Project1
{
    public class ChannelConfiguration
    {
        private const double MAX_VOLTAGE = 10.0;
        private const double MIN_VOLTAGE = 0.0;

        private double driverHiVoltage;
        private double driverLoVoltage;
        private double detectThresholdVoltage;

        public double DriverHiVoltage
        {
            get { return driverHiVoltage; }
            set
            {
                if (value > MAX_VOLTAGE)
                    throw new ArgumentOutOfRangeException(String.Format("Voltage setting cannot be above max value of {0}", MAX_VOLTAGE));
                else if (value < MIN_VOLTAGE)
                    throw new ArgumentOutOfRangeException(String.Format("Voltage setting cannot be below min value of {0}", MIN_VOLTAGE));
                else if (value < driverLoVoltage)
                    throw new ArgumentOutOfRangeException("Voltage setting cannot be below Driver Low Voltage");
                else
                    driverHiVoltage = value;
            }
        }

        public double DriverLoVoltage
        {
            get { return driverLoVoltage; }
            set
            {
                if (value > MAX_VOLTAGE)
                    throw new ArgumentOutOfRangeException(String.Format("Voltage setting cannot be above max value of {0}", MAX_VOLTAGE));
                else if (value < MIN_VOLTAGE)
                    throw new ArgumentOutOfRangeException(String.Format("Voltage setting cannot be below min value of {0}", MIN_VOLTAGE));
                else if (value > driverHiVoltage)
                    throw new ArgumentOutOfRangeException("Voltage setting cannot be above Driver Hi Voltage");
                else
                    driverLoVoltage = value;
            }
        }

        public double DetectThresholdVoltage
        {
            get { return detectThresholdVoltage; }
            set
            {
                if (value > MAX_VOLTAGE)
                    throw new ArgumentOutOfRangeException(String.Format("Voltage setting cannot be above max value of {0}", MAX_VOLTAGE));
                else if (value < MIN_VOLTAGE)
                    throw new ArgumentOutOfRangeException(String.Format("Voltage setting cannot be below min value of {0}", MIN_VOLTAGE));
                else
                    detectThresholdVoltage = value;
            }
        }

        public ChannelConfiguration()
        {
            driverHiVoltage = 5.0;
            driverLoVoltage = 0.8;
            detectThresholdVoltage = 2.2;
        }
    }
}
