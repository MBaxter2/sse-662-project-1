﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Xml.Serialization;

namespace Sse662Project1
{
    public class ObservablePatterns : INotifyPropertyChanged
    {
        public int Number
        {
            get { return number; }
            set
            {
                number = value;
                OnPropertyChanged("Number");
            }
        }

        public ObservableCollection<PatternConfiguration> Patterns
        {
            get { return patterns; }
            set
            {
                patterns = value;
                OnPropertyChanged("Patterns");
            }
        }

        private int number;
        private ObservableCollection<PatternConfiguration> patterns;

        public ObservablePatterns()
        {
            Number = 0;
            Patterns = new ObservableCollection<PatternConfiguration>();
        }

        public ObservablePatterns(int number, int count, LogicLevel initialValue)
        {
            Number = number;
            Patterns = new ObservableCollection<PatternConfiguration>();

            for (int i = 0; i < count; i++)
                Patterns.Add(new PatternConfiguration(initialValue));
        }

        #region INotifyPropertyChanged Implementation
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string name)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
        #endregion
    }
}
