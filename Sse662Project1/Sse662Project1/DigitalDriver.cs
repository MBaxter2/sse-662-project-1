﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sse662Project1
{
    public enum TestResult
    {
        NotTested,
        Failed,
        Passed
    }

    public class DigitalDriver
    {
        public const int CHANNEL_COUNT = 5;
        public const int PATTERN_COUNT = 10;

        private const double PERIOD_MIN = 1.0;
        private const double PERIOD_MAX = 1000000;

        const string SIM_FILE_FOLDER = @"..\..\Simulation Files\";

        private ChannelConfiguration[] channels;
        private PatternConfiguration[,] patterns;
        private double period;

        private TestResult[,] results;

        public DigitalDriver()
        {
            channels = new ChannelConfiguration[CHANNEL_COUNT];
            patterns = new PatternConfiguration[CHANNEL_COUNT, PATTERN_COUNT];
            results = new TestResult[CHANNEL_COUNT, PATTERN_COUNT];

            for (int i = 0; i < CHANNEL_COUNT; i++)
                channels[i] = new ChannelConfiguration();

            for (int i = 0; i < CHANNEL_COUNT; i++)
                for (int j = 0; j < PATTERN_COUNT; j++)
                    patterns[i, j] = new PatternConfiguration();

            for (int i = 0; i < CHANNEL_COUNT; i++)
                for (int j = 0; j < PATTERN_COUNT; j++)
                    results[i, j] = TestResult.NotTested;

            period = 1000;
        }

        public void SetChannelDriverVoltageHi(int channel, double voltage)
        {
            if(channel >= CHANNEL_COUNT || channel < 0)
                throw new ArgumentOutOfRangeException("Invalid channel number");

            channels[channel].DriverHiVoltage = voltage;
        }

        public void SetChannelDriverVoltageLo(int channel, double voltage)
        {
            if(channel >= CHANNEL_COUNT || channel < 0)
                throw new ArgumentOutOfRangeException("Invalid channel number");

            channels[channel].DriverLoVoltage = voltage;
        }

        public void SetChannelDetectVoltage(int channel, double voltage)
        {            
            if(channel >= CHANNEL_COUNT || channel < 0)
                throw new ArgumentOutOfRangeException("Invalid channel number");

            channels[channel].DetectThresholdVoltage = voltage;
        }

        public void SetPatternPeriod(double period)
        {
            if(period > PERIOD_MAX)
                throw new ArgumentOutOfRangeException(String.Format("Period setting cannot be above max value of {0}", PERIOD_MAX));
            if (period < PERIOD_MIN)
                throw new ArgumentOutOfRangeException(String.Format("Period setting cannot be below min value of {0}", PERIOD_MIN));
            else
                this.period = period;
        }

        public void SetChannelDriverLogic(int channel, int index, LogicLevel level)
        {
            if (channel >= CHANNEL_COUNT || channel < 0)
                throw new ArgumentOutOfRangeException("Invalid channel number");

            if (index >= PATTERN_COUNT || index < 0)
                throw new ArgumentOutOfRangeException("Invalid pattern index");

            patterns[channel, index].DriverLevel = level;
        }

        public void SetChannelDetectLogic(int channel, int index, LogicLevel level)
        {
            if (channel >= CHANNEL_COUNT || channel < 0)
                throw new ArgumentOutOfRangeException("Invalid channel number");

            if (index >= PATTERN_COUNT || index < 0)
                throw new ArgumentOutOfRangeException("Invalid pattern index");

            patterns[channel, index].DetectLevel = level;
        }

        public int Execute()
        {
            var sim = new ResponseSimulator(CHANNEL_COUNT, PATTERN_COUNT);

            sim.LoadSimulationFile(SIM_FILE_FOLDER + "simulation.xml");

            int failures = 0; 
            for (int i = 0; i < CHANNEL_COUNT; i++)
                for (int j = 0; j < PATTERN_COUNT; j++)
                    if (patterns[i, j].DetectLevel == LogicLevel.High)
                        if(sim.Data[i, j] > channels[i].DetectThresholdVoltage)
                            results[i, j] = TestResult.Passed; 
                        else
                        {
                            results[i, j] = TestResult.Failed; 
                            failures++;
                        }
                    else if (patterns[i, j].DetectLevel == LogicLevel.Low)                        
                        if(sim.Data[i, j] <= channels[i].DetectThresholdVoltage)
                            results[i, j] = TestResult.Passed; 
                        else
                        {
                            results[i, j] = TestResult.Failed; 
                            failures++;
                        }
                    else
                        results[i, j] = TestResult.NotTested;
            
            return failures;
        }

        public TestResult GetChannelResult(int channel, int index)
        {
            if (channel >= CHANNEL_COUNT || channel < 0)
                throw new ArgumentOutOfRangeException("Invalid channel number");

            if (index >= PATTERN_COUNT || index < 0)
                throw new ArgumentOutOfRangeException("Invalid pattern index");

            return results[channel, index];
        }
    }
}
