﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Sse662Project1
{
    public class ResponseSimulator
    {
        private double[,] data;
        private int patterns = 0;
        private int channels = 0;

        public double[,] Data
        {
            get { return data; }
        }

        public ResponseSimulator(int channels, int patterns)
        {
            data = new double[channels, patterns];

            this.channels = channels;
            this.patterns = patterns;

            for(int i = 0; i < channels; i++)
                for(int j = 0; j < patterns; j++)
                    data[i, j] = 0;
        }

        public void LoadSimulationFile(string file)
        {
            int i;
            int j;
            double value;
            bool parseResult;
            XmlDocument doc = new XmlDocument();

            doc.Load(file);

            i = 0;
            XmlNode root = doc.ChildNodes[0];
            if (root.Name != "simulation")
                throw new SimulatorFileException("Invalid simulation file format");

            foreach (XmlNode pattern in root.ChildNodes)
            {
                if(pattern.Name == "pattern")
                {
                    if (i >= patterns)
                        throw new SimulatorFileException("File exceeds number of patterns");
                    
                    j = 0;
                    foreach(XmlNode channel in pattern.ChildNodes)
                    {
                        if(channel.Name == "channel")
                        {
                            if (j >= channels)
                                throw new SimulatorFileException("File exceeds number of channels");

                            parseResult = Double.TryParse(channel.InnerText, out value);
                            if (parseResult == false)
                                throw new SimulatorFileException("Invalid channel output value");
                            else
                                data[j, i] = value;
                        }
                        else
                            throw new SimulatorFileException("Invalid simulation file format");

                        j++;
                    }
                }
                else
                    throw new SimulatorFileException("Invalid simulation file format");

                i++;
            }
        }
    }

    public class SimulatorFileException : Exception
    {
        public SimulatorFileException(string message) : base(message)
        {
        }
    }
}
