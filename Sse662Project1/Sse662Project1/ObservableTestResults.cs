﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Collections.ObjectModel;

namespace Sse662Project1
{
    public class ObservableTestResults : INotifyPropertyChanged
    {
        public int Number
        {
            get { return number; }
            set
            {
                number = value;
                OnPropertyChanged("Number");
            }
        }

        public ObservableCollection<TestResult> Results
        {
            get { return results; }
            set
            {
                results = value;
                OnPropertyChanged("Results");
            }
        }

        private int number;
        private ObservableCollection<TestResult> results;

        public ObservableTestResults(int number, int count, TestResult initialValue)
        {
            Number = number;
            Results = new ObservableCollection<TestResult>();

            for (int i = 0; i < count; i++)
                Results.Add(initialValue);
        }

        #region INotifyPropertyChanged Implementation
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string name)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
        #endregion
    }
}
