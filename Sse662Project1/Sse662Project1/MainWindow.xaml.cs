﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Sse662Project1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var programmer = this.FindResource("programmer") as InstrumentProgrammer;
            programmer.RunConfiguration();
        }

        private void MenuItem_Exit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void MenuItem_Save_Click(object sender, RoutedEventArgs e)
        {
            var programmer = this.FindResource("programmer") as InstrumentProgrammer;
            programmer.SaveConfiguration();
        }

        private void MenuItem_Load_Click(object sender, RoutedEventArgs e)
        {
            var programmer = this.FindResource("programmer") as InstrumentProgrammer;
            programmer.LoadConfiguration();
        }
    }
}
