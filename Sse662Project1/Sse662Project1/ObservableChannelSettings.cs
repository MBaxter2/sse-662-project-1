﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Collections.ObjectModel;

namespace Sse662Project1
{
    public class ObservableChannelSettings : INotifyPropertyChanged
    {
        public int Number
        {
            get { return number; }
            set
            {
                number = value;
                OnPropertyChanged("Number");
            }
        }

        public double High
        {
            get { return high; }
            set
            {
                high = value;
                OnPropertyChanged("High");
            }
        }

        public double Low
        {
            get { return low; }
            set
            {
                low = value;
                OnPropertyChanged("Low");
            }
        }

        public double Detect
        {
            get { return detect; }
            set
            {
                detect = value;
                OnPropertyChanged("Detect");
            }
        }

        private int number;
        private double high;
        private double low;
        private double detect;

        public ObservableChannelSettings(int number, double high, double low, double detect)
        {
            Number = number;
            High = high;
            Low = low;
            Detect = detect;
        }
        
        #region INotifyPropertyChanged Implementation
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string name)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
        #endregion
    }
}
