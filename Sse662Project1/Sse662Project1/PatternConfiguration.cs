﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace Sse662Project1
{
    public enum LogicLevel
    {
        Disabled,
        Low,
        High
    }

    public class PatternConfiguration : INotifyPropertyChanged
    {
        private LogicLevel driverLevel;
        private LogicLevel detectLevel;

        public LogicLevel DriverLevel
        {
            get { return driverLevel; }
            set 
            {
                driverLevel = value;
                OnPropertyChanged("DriverLevel");
            }
        }

        public LogicLevel DetectLevel
        {
            get { return detectLevel; }
            set 
            { 
                detectLevel = value;
                OnPropertyChanged("DetectLevel");
            }
        }

        public PatternConfiguration()
        {
            DriverLevel = LogicLevel.Disabled;
            DetectLevel = LogicLevel.Disabled;
        }

        public PatternConfiguration(LogicLevel initialValue)
        {
            DriverLevel = initialValue;
            DetectLevel = initialValue;
        }

        #region INotifyPropertyChanged Implementation
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string name)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
        #endregion
    }
}
