﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Xml.Serialization;
using System.IO;

namespace Sse662Project1
{
    public class InstrumentProgrammer : INotifyPropertyChanged
    {
        #region Interface Properties
        public ObservableCollection<ObservableChannelSettings> ChannelVoltages
        {
            get { return channelVoltages; }
            set
            {
                channelVoltages = value;
                OnPropertyChanged("ChannelVoltages");
            }
        }

        public double Period
        {
            get { return period; }
            set
            {
                period = value;
                OnPropertyChanged("Period");
            }
        }

        public ObservableCollection<ObservablePatterns> ChannelLogic
        {
            get { return channelLogic; }
            set
            {
                channelLogic = value;
                OnPropertyChanged("ChannelLogic");
            }
        }

        public ObservableCollection<ObservableTestResults> ChannelResults
        {
            get { return channelResults; }
            set
            {
                channelResults = value;
                OnPropertyChanged("ChannelResults");
            }
        }

        public string LastError
        {
            get { return lastError; }
            set
            {
                lastError = value;
                OnPropertyChanged("LastError");
            }
        }
        #endregion

        private DigitalDriver driver;

        // Interface adjustable driver settings
        private ObservableCollection<ObservableChannelSettings> channelVoltages;
        private double period;
        private ObservableCollection<ObservablePatterns> channelLogic;
        private ObservableCollection<ObservableTestResults> channelResults;
        private string lastError;

        // Interface default values
        private const double DEFAULT_HI_VOLTAGE = 5.0;
        private const double DEFAULT_LO_VOLTAGE = 1.0;
        private const double DEFAULT_DETECT_VOLTAGE = 2.5;
        private const double DEFAULT_PERIOD = 100;
        private const LogicLevel DEFAULT_LOGIC = LogicLevel.Disabled;

        public InstrumentProgrammer()
        {
            LastError = "";
            driver = new DigitalDriver();

            Period = DEFAULT_PERIOD;

            ChannelVoltages = new ObservableCollection<ObservableChannelSettings>();
            ChannelLogic = new ObservableCollection<ObservablePatterns>();
            ChannelResults = new ObservableCollection<ObservableTestResults>();

            for(int i = 0; i < DigitalDriver.CHANNEL_COUNT; i++)
            {
                ChannelVoltages.Add(new ObservableChannelSettings(i+1, DEFAULT_HI_VOLTAGE, DEFAULT_LO_VOLTAGE, DEFAULT_DETECT_VOLTAGE));
                ChannelLogic.Add(new ObservablePatterns(i+1, DigitalDriver.PATTERN_COUNT, DEFAULT_LOGIC));
                ChannelResults.Add(new ObservableTestResults(i+1, DigitalDriver.PATTERN_COUNT, TestResult.NotTested));
            }
        }

        public void RunConfiguration()
        {
            LastError = "";

            try
            {
                // Configure instrument
                for(int i = 0; i < DigitalDriver.CHANNEL_COUNT; i++)
                {
                    driver.SetChannelDriverVoltageHi(i, ChannelVoltages[i].High);
                    driver.SetChannelDriverVoltageLo(i, ChannelVoltages[i].Low);
                    driver.SetChannelDetectVoltage(i, ChannelVoltages[i].Detect);
                    driver.SetPatternPeriod(Period);

                    for(int j = 0; j < DigitalDriver.PATTERN_COUNT; j++)
                    {
                        driver.SetChannelDriverLogic(i, j, ChannelLogic[i].Patterns[j].DriverLevel);
                        driver.SetChannelDetectLogic(i, j, ChannelLogic[i].Patterns[j].DetectLevel);
                    }
                }

                // Run configuration
                int result = driver.Execute();

                // Get results
                for (int i = 0; i < DigitalDriver.CHANNEL_COUNT; i++)
                {
                    for(int j = 0; j < DigitalDriver.PATTERN_COUNT; j++)
                    {
                        ChannelResults[i].Results[j] = driver.GetChannelResult(i, j);
                    }
                }
             }
            catch(ArgumentOutOfRangeException ex)
            {
                LastError = ex.Message;
            }
            catch(SimulatorFileException ex)
            {
                LastError = ex.Message;
            }
        }

        public void SaveConfiguration()
        {
            var serializer = new XmlSerializer(channelLogic.GetType());

            using(var file = new FileStream(@"Patterns.xml", FileMode.Create))
            using(var writer = new StreamWriter(file))
            {
                serializer.Serialize(writer, channelLogic);
            }
        }

        public void LoadConfiguration()
        {
            var serializer = new XmlSerializer(channelLogic.GetType());

            using (var file = new FileStream(@"Patterns.xml", FileMode.Open))
            using (var reader = new StreamReader(file))
            {
                ChannelLogic = serializer.Deserialize(reader) as ObservableCollection<ObservablePatterns>;
            }
        }

        #region INotifyPropertyChanged Implementation
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string name)
        {
            var handler = PropertyChanged;            
            if(handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
        #endregion
    }
}
